'''
	updload_firmware.py

	\author Charlie Miller
	\brief Parser used to upload a given firmware file to our remote Firebase Realtime Database 
		for use with the Hyperion Android App

	\usage: python upload_firmware.py [filename.bin]
	
	Adding a description: 
		<code> python upload_firmware [filename.bin] -d "Firmware description" </code>
	
	Don't want the build number to be reflected by the file name? Set it manually with:
		<code> python_updload_firmware [filename.bin] -b 4840 </code>
		\note This argument MUST be a digit.
	
	\TODO Add prompts for the user.
	\TODO Add the ability to generate a skeleton json file, and a targetable json file parameter.

'''

import sys, getopt
import pyrebase 
import datetime
import zlib
import os.path

# Credentials. Don't really care about the secrecy, atm.
email = "charliea.miller@gmail.com"
passwrd = "GDURECsz9wTqWtWQ"

# Firebase configurations
config = {
	"apiKey": "AIzaSyB1VT83Jg14RHL9gqheA9tS4OjCeq903hY",
	"authDomain": "NONE",
	"databaseURL": "https://hyperion-8af78.firebaseio.com",
	"storageBucket": "hyperion-8af78.appspot.com"
}

'''
	\param fileName 	The filename of the file we wish to determine the crc of.
	\returns A formatted string that represents the CRC32 of the given file
'''
def crc(fileName):
    prev = 0
    for eachLine in open(fileName,"rb"):
        prev = zlib.crc32(eachLine, prev)
    return "%X"%(prev & 0xFFFFFFFF)

# Make sure we have at least one usable arg.
if len(sys.argv) > 1:

	''' 
		Database info 
	'''
	description = "Descr"
	build_version = ""
	hardware_versions = [""]
	file_size = 0
	release_date = datetime.date.today().strftime("%m/%d/%y")
	release_notes = "N/A"
	url = ""
	current_build = 1
	crc32 = ""
	channel = "rel"
	name = ""
	'''
		/Database info
	'''
	filename = ""
	
	# Commands that can be added to the command line.
	# TODO: Add description as a prompt to the user.
	optionalCommands = {
		"-d": description,
		 "-c": channel}

	# Print the help prompt.
	# Yeah, it's dumb I didn't add this to optional commands,
	# but it would've been more work to handle its case later on.
	if "-h" in sys.argv:

		print "-h: HELP <arg arg> gives usage for another arg"
		print "-d: DESCRIPTION; <arg description> Text that details the firmware being uploaded."
		print "-c: CHANNEL; <arg channel> values = 'dev' or 'rel' defaults to 'rel'"
		print "-b: BUILD VERSION; <arg build> the build number for the firmware file."

	# Filename has to be the first parameter
	# We check if it's a 
	if os.path.isfile(sys.argv[1]):
		
		filename = sys.argv[1]

		file_size = os.path.getsize(filename)
		crc32 = crc(filename)

		if "-b" in sys.argv:
			commandIndex = sys.argv.index("-b")
			
			if commandIndex < len(sys.argv) + 1:

				arg = sys.argv[commandIndex + 1]

				if str(arg).isdigit():
					build_version = int(arg)

				else:
					print "BUILD NUMBER MAY ONLY BE COMPRISED OF DIGITS"

			else:
				print "YOU MUST PROVIDE AN ARGUMENT"

		else:

			build_version = str(filename)

			build_version = build_version.replace(".bin", "")
			name = build_version
			build_version = build_version.replace("fw", "")

			if "d" in build_version:
				build_version = build_version.replace("d", "")
				channel = "dev"
			
			if build_version.isdigit():
				build_version = int(build_version)
			else:
				print "Was unable to automatically retrieve the build number from the file name. Please re-run application with '-b' parameter"

		# Parse args for optional commands
		for arg in sys.argv:

			if arg in optionalCommands.keys():
				commandIndex = sys.argv.index(arg)
				if commandIndex < len(sys.argv) + 1:
					second_arg = sys.argv[commandIndex + 1]
					optionalCommands[arg] = second_arg 
				pass


		# Assign optional arguments
		description = optionalCommands["-d"]
		channel = optionalCommands["-c"]

		# Setup firebase
		firebase = pyrebase.initialize_app(config)
		auth = firebase.auth()
		user = auth.sign_in_with_email_and_password(email, passwrd)
		storage = firebase.storage()
		db = firebase.database()
		
		# Push file to remote Storage.
		storage.child(filename).put(filename)

		# Retrieve the url of the remote firmware file.
		url = storage.child(filename).get_url(None)

		# Create dictionary for our data that we'll push 
		# to our database.
		db_map = {
				"CRC32": str(crc32),
				"buildNumber": int(build_version),
				"channel": str(channel),
				"currentBuild": int(1),
				"description": str(description),
				"fileSize": int(file_size),
				"name": str(name),
				"releaseDate": str(release_date),
				"releaseNotes": "",
				"url": str(url)
			}

		# Show user the data
		print db_map

		# Update the database, the firmware will now be viewable to the app.
		db.child("firmware").child(name).set(db_map, user['idToken'])

	else:
		print "NO FIRMWARE FILE WAS GIVEN"
